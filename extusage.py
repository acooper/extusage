from aiolimiter import AsyncLimiter
import aiohttp
import base64
import csv
import asyncio
import json
import re
import sys

# Adjust rate limit if getting too many 429 response codes
# rate limit of 20 requests per second and max 50 concurrent requests
MAX_CONCURRENT = 50
RATE_LIMIT_IN_SECOND = 20
rate_limit = AsyncLimiter(RATE_LIMIT_IN_SECOND, 1.0)

async def fetch(session, url):
    async with session.get(url) as response:
        response.raise_for_status()
        body = await response.text()
        # some wikis like wikidata have some extra attributes before href so skip over them with [^>]*
        return re.findall(r'<a class=\"external mw-version-ext-name\" [^>]*href=\"([^"]+)\"', body)

async def fetch_all(loop):
    urls = await get_wiki_urls()
    async with rate_limit:
        async with aiohttp.ClientSession(loop=loop) as session:
            return await asyncio.gather(*[fetch(session, "https://%s/wiki/Special:Version" % (url)) for url in urls], return_exceptions=True)

def count_extensions(ext_lists):
    ext_count = {}
    for exts in ext_lists:
        # results include embedded exception records
        if not isinstance(exts, list):
            sys.stderr.write(str(exts)+'\n')
            continue
        for ext in exts:
            if ext not in ext_count:
                ext_count[ext] = 1
            else:
                ext_count[ext]+= 1
    return ext_count

def output(ext_count):
    with open('ext_count.csv', 'w', newline='') as csvfile:
        countwriter = csv.writer(csvfile)
        countwriter.writerow(['Extension', 'Frequency'])
        for ext in ext_count:
            countwriter.writerow([ ext, ext_count[ext]])
        

async def get_wiki_urls():
    # get list of wikis from parsoid library (seems to be a complete list?)
    async with aiohttp.ClientSession() as session:
        async with session.get('https://meta.wikimedia.org/w/api.php?action=sitematrix&format=json') as resp:
            resp.raise_for_status()
            wikilist = json.loads(await resp.text())
            try:
                wikicount = int(wikilist["sitematrix"]["count"])
            except KeyError:
                raise Exception("Unable to parse count from wiki list wmf.sitematrix.json, check URL contents match expected schema in code")
            # bit hacky, rather than process the json schema just grab and return all URLs
            urls = re.findall(r'\"https://([^"]+)\"', json.dumps(wikilist))
            if len(urls) != wikicount:
                raise Exception("Did not find expected number of wiki URLs %s (found %s)" % (wikicount, len(urls)))
            return urls

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    ext_lists = loop.run_until_complete(fetch_all(loop))
    ext_count = count_extensions(ext_lists)
    output(ext_count)