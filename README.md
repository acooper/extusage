Gathers the number of extensions enabled on public Wikis supported by WMF infrastructure.

Output is written to a csv file ext_count.csv in the current directory.

A list of wiki URL is obtained from SiteMatrix API endpoint. asyncio is then used to relatively efficiently lookup Special:Version for this large number of sites.

Running this script may generate Too many open files errors on stdout.
To fix this run 'ulimit -n 10240' in the shell before executing the script.

Rate limiting is in place. 429 HTTP response codes will be output to stdout.
If there is too many of these, adjust the rate limit controls in the script.
The following represents a rate limit of 20 requests per second and max 50 concurrent requests.

	MAX_CONCURRENT = 50
	RATE_LIMIT_IN_SECOND = 20

Alternative place to get data: https://wikistats.wmcloud.org/
Also see info here: https://meta.wikimedia.org/wiki/Wikimedia_wikis
